import json
import boto3
from botocore.exceptions import ClientError

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('Todos')

def lambda_handler(event, context):
    try:
        # Retrieve the 'id' from pathParameters instead of the request body
        id = event['pathParameters']['id']
        
        # Parse the request body to get the 'title'
        body = json.loads(event['body'])
        title = body['title']
        
        # Update the item in DynamoDB
        response = table.update_item(
            Key={'id': id},
            UpdateExpression='SET title = :val',
            ExpressionAttributeValues={':val': title},
            ReturnValues="UPDATED_NEW"
        )
        
        # Return a successful response with the updated attributes
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            'body': json.dumps({'message': 'Todo updated', 'updatedAttributes': response['Attributes']})
        }
    except ClientError as e:
        print(e.response['Error']['Message'])
        return {
            'statusCode': 500,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            'body': json.dumps({'error': e.response['Error']['Message']})
        }
