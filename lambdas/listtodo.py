import json
import boto3
from botocore.exceptions import ClientError

# Initialize a DynamoDB resource using boto3
dynamodb = boto3.resource('dynamodb')
# Reference the DynamoDB table
table = dynamodb.Table('Todos')

def lambda_handler(event, context):
    try:
        # Scan the table to retrieve all items
        response = table.scan()
        items = response['Items']
        
        # Return a successful response with the items serialized as a JSON string
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',  # Ensure the Content-Type is set to application/json
                'Access-Control-Allow-Origin': '*',  # Optional: Include this if you're dealing with CORS in a web context
            },
            'body': json.dumps(items)  # Serialize the list of items to a JSON string
        }
    except ClientError as e:
        # Log the error to CloudWatch Logs
        print(e.response['Error']['Message'])
        
        # Return an error response
        return {
            'statusCode': 500,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',  # Optional: Include for CORS
            },
            'body': json.dumps({'error': e.response['Error']['Message']})
        }

