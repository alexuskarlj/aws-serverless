import json
import boto3
from botocore.exceptions import ClientError

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('Todos')

def lambda_handler(event, context):
    try:
        # Retrieve the 'id' from pathParameters instead of the request body
        id = event['pathParameters']['id']
        
        # Delete the item from DynamoDB using the retrieved 'id'
        table.delete_item(Key={'id': id})
        
        # Return a successful response indicating the todo was deleted
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            'body': json.dumps({'message': 'Todo deleted'})
        }
    except ClientError as e:
        # Log the error message and return a server error response
        print(e.response['Error']['Message'])
        return {
            'statusCode': 500,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            'body': json.dumps({'error': e.response['Error']['Message']})
        }
