import React, { useState } from 'react';
import axios from 'axios';
import '../App.css';

function AddTodo() {
  const [title, setTitle] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('https://0ckkt2v8ui.execute-api.ap-southeast-1.amazonaws.com/todos', {
        title: title
      });
      console.log(response.data); 
      setTitle(''); 
    } catch (error) {
      console.error('Error adding todo:', error);
    }
  };

  return (
    <form className="AddTodo" onSubmit={handleSubmit}>
      <input
        type="text"
        value={title}
        onChange={e => setTitle(e.target.value)}
        placeholder="Write a Todo"
      />
      <button type="submit">ADD TODO</button>
    </form>
  );
}

export default AddTodo;