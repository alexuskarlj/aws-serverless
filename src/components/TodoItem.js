import React from 'react';

function TodoItem({ todo, onDelete }) {
  return (
    <div className="todo-item">
      <span className="todo-title">{todo.title}</span>
      <button onClick={() => onDelete(todo.id)}>Delete</button>
    </div>
  );
}

export default TodoItem;