import React from 'react';
import './App.css';
import AddTodo from './components/AddTodo';
import TodoList from './components/TodoList';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Simple TODO App
      </header>
      <AddTodo />
      <TodoList />
    </div>
  );
}

export default App;